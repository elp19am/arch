// Crash event:
1. After it crashes open it again.
2. click 'New'. It will tell you that autosave files exist. select 'yes' to restore them.

// When opening old projects:
1. if video files are missing, select replace with placeholders
2. before you do anything, right click video files, click replace
3. select the same video files

# videos note: 
either keep videos in original place or store them in specific folder (eg. Documents/kdenlive); otherwise project will be corrupted.

# separate audio from video:
right click video track -> ungroup

// cut clip:
x: cutting tool
s: selection tool

# blur face: 
right click video track -> add effect -> blur and hide
resize as you wish. you can remove height & width linking.

# zoom in/out:
ctrl + scroll

// fade video:
To fade video to black, add (fade to/from black transition) from Effects tab. or just drag from the edge of a video track (like audio)

# fade audio:
just drag from the edge of an audio track

# add subtitles: (use Times font, not Arial)
project -> add title -> drag and drop it into timeline

// add black gap:
Project -> 'Add colour clip'

# drag clips/make space in middle:
hold shift then click and drag to select more than one clip at one time, then drag them forward to make space.

# insert track/audio/video in timeline:
right click blocks on left -> insert

// zoom photos/transitions:
use 'effects' tab on the top left area, search for 'Position and Zoom'
add 'keyframes' and set zoom level and focus center location at each keyframe

// full screen not working:
make sure the laptop monitor is not connected, as the full screen uses the second monitor if it exists.

// speed effect (fastforward):
right click video > speed

