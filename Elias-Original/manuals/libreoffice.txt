// Fix the Arabic language orientation:

Enable Complex Text Layout (CTL) here:
Tools -> Options -> Language settings -> Languages

Then do: 
ctrl + rightshift to switch to right orientation
crtl + leftshift to switch to left orientation

On Mac:
Shift+CMD+A
Shift+CMD+D

see:
https://help.libreoffice.org/Common/General_Glossary#Complex_Text_Layout_.28CTL.29
https://help.libreoffice.org/Common/Languages_Using_Complex_Text_Layout
https://superuser.com/questions/543559/how-to-change-the-text-direction-in-libreoffice

// change default fonts:
settings -> libre office writer -> basic fonts

// enter special characters:
1. hold down control+shift then press 'u'
2. enter character code, eg. 06a4 then press enter
# or (also works in word)
type the text "06a4", then highlight it and press alt+X

// download dictionary:
https://extensions.libreoffice.org/en/extensions/show/english-dictionaries
Add the downloaded file: Tools > Extension manager > add

