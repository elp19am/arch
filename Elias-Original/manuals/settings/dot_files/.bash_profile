#
# ~/.bash_profile (this file is run by a login shell; unlike bashrc, which is run by an interactive shell). It runs only when logging in from a shell, not LXDM. For graphical login, ~/.profile is executed
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

. ~/.profile

