#
# ~/.bashrc
#

# <<< the code here is executed everytime terminal is launched.. >>> 

# <<<<<< ELIAS >>>>>>
# source my commands:
source ~/.sourcer
# source /opt/ros/kinetic/setup.bash
# xbindkeys # bind key shortcuts

#change title of command request:
MAGENTA="\[$(tty -s && tput setaf 125)\]"; # note that the [] are used to solve loss of precision when typing in terminal; tty -s means run only when tty is running (this is to avoid error at startup)
ORANGE="\[$(tty -s && tput setaf 166)\]";
GREEN="\[$(tty -s && tput setaf 64)\]";
PURPLE="\[$(tty -s && tput setaf 61)\]";
GREY="\[$(tty -s && tput setaf 244)\]";
WHITE="\[$(tty -s && tput setaf 254)\]";
resetcolours="\[$(tty -s && tput sgr0)\]"

PS1="${MAGENTA}\u" # or: PS1="$(tput setaf 125)\u"
PS1+="${WHITE} @ "
PS1+="${ORANGE}\h"
PS1+="${WHITE} in "
PS1+="${GREEN}\w"
PS1+="${PURPLE}\$ "
PS1+="${resetcolours}"
# \u means display my name;
# \w working dir;
# \W folder name;
# \t current time;
# \h is host name
# default value is: "\u@\h \W -> "
# source: https://www.youtube.com/watch?v=LXgXV7YmSiU

# change colours:
# $(tput setaf 166) coloured text $(tput sgr0) # what is 
# clour codes: https://upload.wikimedia.org/wikipedia/commons/1/15/Xterm_256color_chart.svg
# https://www.youtube.com/watch?v=v1S5J39-93I

# possibly:
# elias @ elias-pc in ~/ on (master)
# $>
export PS1;

# Install Ruby Gems to ~/gems
#export GEM_HOME="$HOME/gems"
#export PATH="$HOME/gems/bin:$PATH"
