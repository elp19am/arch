export EDITOR=/usr/bin/vim

# this file is run when logging in graphically (eg. LXDM/lightdm); unlike
#  ~/.bash_profile

picom --config ~/.config/picom.conf & # launch shadow compositor

setxkbmap gb # set keyboard layout, awesome sets its own us keyboard on startup

#parcellite & # -n & # copy and paste on awesomewm. ctrl+alt+p for settings
#xclip &
clipit -n &

nm-applet & # network manager icon in system tray.

#lxsession & # required for pamac &
#              starts nm-applet (wired connection widget) & 
#              starts lxclipboard (copy and paste across windows on awesomewm)
# A session manager is used to automatically start a set of applications and set
#  up a working desktop environment. Moreover, when the user logout, it remembers
#  the applications in use, and restart them the next time you login.
#  https://askubuntu.com/questions/196721/what-is-lxsession

#lxpolkit & # required for pamac authentication. Not required for other
#             authentication, it can be done from terminal.

# On Lenovo only:
cbatticon -u 5 -i notification -c "systemctl suspend" -l 20 -r 3 &

# blue light filter (either use [redshift] or [blugon]):
# redshift -P -O 3000 &
blugon -S 3900 -o # + remove all blue colour from monitor.

# Disable screen sleeping:
# xset is the xorg settings manager
xset -b         # turn off bell (beeping)
xset dpms 0 0 0 # set power saving times to infinity
xset -dpms      # turn off display power saving
xset s off      # turn off screen saver
#xset q         # display current settings

# Backup firefox profile on each restart:
#if [ ! -d "$DIRECTORY" ]; then
#  mkdir ~/.cache/firefox_backups
#else

# Considerably slows down firefox:
#rm -rf ~/.cache/firefox_backups
#mkdir  ~/.cache/firefox_backups
#fi
#cp -r /home/elias/.mozilla/firefox ~/.cache/firefox_backups/

#export EDITOR=/usr/bin/nano
#export QT_QPA_PLATFORMTHEME="qt5ct"
#export QT_AUTO_SCREEN_SCALE_FACTOR=0
#export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

# Decrease window resolution (uni monitor)
#xrandr --newmode "1600x900_60.00"  118.25  1600 1696 1856 2112  900 903 908 934 -hsync +vsync
#xrandr --addmode VGA1 "1600x900_60.00"
#xrandr -d :0 --output VGA1 --mode 1600x900_60.00

# Mirror external monitor same as Lenovo monitor: (run [xrandr --current] to display monitor names)
xrandr --output HDMI2 --output eDP1 --same-as HDMI2
# https://wiki.archlinux.org/index.php/Multihead#Configuration_using_xrandr

# Increase mouse scrolling speed: https://wiki.archlinux.org/index.php/Libinput#Mouse_wheel_scrolling_speed_scaling
echo 3 > /tmp/libinput_discrete_deltay_multiplier

# Wallpaper:
sleep 0.1 && ~/.fehbg & # set wallpaper on awesome. the delay is required otherwise awesome will set its own wallpaper

# <<<<<< from ubuntu: >>>>>>

# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
#if [ -d "$HOME/bin" ] ; then
#    PATH="$HOME/bin:$PATH"
#fi

# set PATH so it includes user's private bin if it exists
#if [ -d "$HOME/.local/bin" ] ; then
#    PATH="$HOME/.local/bin:$PATH"
#fi

