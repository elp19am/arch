:set ruler
" display the ruler

:set laststatus=2
" to expand the status bar

" :set relativenumber
" show line numbers relative to current line
" normal:
:set number

:set title
" show title of file in window title

:set hlsearch
" highlighting text when searching
" note: the above could be written on one line: set ruler number...
    
:set tabstop=4       " number of visual spaces per TAB
:set softtabstop=4   " number of spaces in tab when editing
:set expandtab       " tabs are spaces

:set ignorecase 
" ignore case when searching

:syntax on
" set colouring on

" :set list
" show non printable characters

" syntax colouring for editing code files:
colorscheme default

" add a custom colour scheme:
" colorscheme seagull
" colour space breezy
" set background=dark
" set termguicolors " if you want to run vim in a terminal

" :set mouse=a
" click with mouse to move cursor to click location (this disables copying from file to file)

:set clipboard+=unnamed
" copy mouse selection to clipboard (paste with mouse middle button)

:set autoindent

