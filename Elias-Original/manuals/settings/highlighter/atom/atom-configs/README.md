# [OBSOLETE]

# Custom syntax highlighting for Atom based on my previous highlighter code: /latach/highlighter

# Note: 
This project has moved to the following 3 separate projects:

https://gitlab.com/latach/highlighter2-grammar

https://gitlab.com/latach/Highlighter2-light-syntax

https://gitlab.com/latach/Highlighter2-light-ui

# Install instructions:
cd /Users/elias/.atom/packages
git clone git@gitlab.com:latach/highlighter2-grammar.git
git clone git@gitlab.com:latach/Highlighter2-light-syntax.git
git clone git@gitlab.com:latach/Highlighter2-light-ui.git

# Highlighter2 Text Editor
Custom syntax highlighting for your favourite text editor (Atom). This is a highlighting grammar designed for writing and colouring general notes. The __advantage__ of this editor is that you can edit the resulting text files with Vim; i.e. you will be able to commit your changes using git, you can't do this with any other text editor (for example even when you enable FLAT XML ODF Text Document in Libreoffice). You can even edit the resulting notes on any machine using any text editor!

![Screenshot goes here](https://gitlab.com/latach/Atom-configs/raw/master/screenshots/Screen%20Shot%202018-05-24%20at%2010.12.26%20pm.png)

# ~~I. How to install:~~ [DEPRECATED; follow instructions above instead]
##### ~~From github:~~
~~Install everything (Note: this will replace existing Atom config files and create a whole new text editor):
`make project configurations vim date languagelatex`~~

##### ~~Using Atom package manager (apm):~~
~~Cd to project then:
`apm install highlighter2`
Then copy the colours section from `~/.atom/packages/Highlighter2/configs/styles.less` to your own stylesheet file.~~

#### ~~Project structure:~~
~~Project files exist in:
`/grammars` & `package.json`.
Atom config files are in:
`/configs`~~

-----

# II. Shortcuts:
_Insert date (from date package)_
`alt+d` mac: `ctrl+d`

_Show markdown preview in Atom (package):_
`ctrl+shift+m`

_Show available commands (including ones from packages; this is using a package):_
`ctrl+shift+p` _(or from Packages->packageName)_

_Change current language grammar (using a package too):_
`ctrl+shift+l`

-----

# III. TODO:
- [ ] My aim for the next stage is to merge this code with github's Markdown so that the text may be rendered beautifully without seeing the colouring syntax:

- [x] Move colour definitions from styles.less config file to a separate syntax theme (adding colours to the syntax in the grammar):
https://flight-manual.atom.io/hacking-atom/sections/creating-a-theme/#creating-a-syntax-theme
https://github.com/atom/one-dark-syntax

* Also check:
https://discuss.atom.io/t/how-to-define-colours-to-syntax-defined-in-grammars/54914
https://stackoverflow.com/questions/50195728/how-to-change-the-colours-of-a-grammar-syntax-in-atom-text-editor
https://flight-manual.atom.io/using-atom/sections/basic-customization/#platform-mac

* Colour guide:
https://www.w3schools.com/colors/colors_hsl.asp
https://upload.wikimedia.org/wikipedia/commons/1/15/Xterm_256color_chart.svg

UPDATE: You can Actually use CSS in Markdown:
<span style="color:green">This is Green text</span>

- [ ] Add support for images (online/offline in same folder) and youtube video links just like markdown; also enable the markdown preview and add colours to it; see the following links:
https://daringfireball.net/projects/markdown/ (you can download source for original markdown here)
https://github.com/atom/language-gfm/ (github's flavoured markdown gfm)
https://github.com/atom/markdown-preview

* To create a grammar:
https://flight-manual.atom.io/hacking-atom/sections/creating-a-grammar/#platform-mac
http://manual.macromates.com/en/language_grammars

* Regex writing: (from Atom's 'Creating a Grammar' writing page)
https://www.rexegg.com/regex-quickstart.html
https://www.regular-expressions.info/tutorial.html

* Similar projects:
https://github.com/fabiocolacio/Marker

- [ ] Add support for linking documents (go to location [#Line 50] in [document X]).

- [ ] use \`\` to make a code block with different font (just like markdown)? use it on file paths or code.
- [ ] use \_these\_ to make _italic text_
- [ ] use \_\_these\_\_ to make __bold and coloured text__ (as here: https://github.com/atom/markdown-preview)

- [ ] Make the line comments active only at the beginning of lines, not middle or end.
       
- [ ] Highlight what is between quotation marks " "

-----

# IV. Known bugs (Disadvantages over gedit):
* Atom does not work well on arabic text (https://github.com/atom/atom/issues/17276)
