# Your init script
#
# Atom will evaluate this file each time a new window is opened. It is run
# after packages are loaded/activated and after the previous editor state
# has been restored.
#
# An example hack to log to the console when each text editor is saved.
#
# atom.workspace.observeTextEditors (editor) ->
#   editor.onDidSave ->
#     console.log "Saved! #{editor.getPath()}"

# Disable code folding; can be also done in: /Users/elias/.atom/packages/Highlighter2-light-syntax/styles/editor.less
{TextEditor} = require('atom')
TextEditor.prototype.isFoldableAtBufferRow = -> false
