# highlighter
* A gedit colour scheme that allows adding colours to .txt files.
* Advantage: edit same .txt files in vim (very simple colour codes).

Usage:
======
1. Line highlight:
```
/r red
/g green
/b blue
/p purple
/o orange
/y yellow
/d dark
/w white
// red
#  green
```

2. Area highlight:
```
<r r>
<g g>
etc..
```

3. Symbol highlight:
```
* = $
```

Other:
======

* [gedit.desktop] is a .desktop file that allows opening files with separate windows.
* This works on ubuntu. Will it work on Arch?
* In addition to this file, you have to manually add to the file [~/.local/share/applications/mimeapps.list]:
```
[Default Applications]
text/x-log=geditspecial.desktop
text/plain=geditspecial.desktop

[Added Associations]
text/x-log=geditspecial.desktop;
text/plain=geditspecial.desktop;
```

### DONE: (ADD THESE TO LIST OF FEATURES IN README

1. use different symbols for colours: (DONE)

< r r>

< g g>

etc..

/r red

/g green

/b blue

/p purple

/o orange

/y yellow

/d dark

/w white

---

### TO DO: (All of these are moved to Highlighter2 TODO section in README.md)


  * detect a path [/path/path/...] (see c.lang file) (see how '#include < aaa >' is done)

  * use `` to make a code block with different font?
  * use \_word\_ to make italic
  * use \_\_word\_\_ to make bold (as here: https://github.com/atom/markdown-preview)

[X] + '< + [letter] + [space]' (HAS TO HAVE A SPACE IN IT)

[X] SAME THING IN THE CASE OF '/' + 'r'
  
  * Make the line comments active only at the beginning of lines
    i.e. /r ..... valid
    
    This is a new line /r ....... invalid

[X] add $ to the pink symbols
  
