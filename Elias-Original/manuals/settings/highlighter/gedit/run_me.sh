#!/bin/bash

if [ ! -d "~/.local/share/gtksourceview-4/" ]; then
	mkdir -p ~/.local/share/gtksourceview-4/
fi

if [ ! -d "~/.local/share/gtksourceview-4/language-specs/" ]; then
	mkdir -p ~/.local/share/gtksourceview-4/language-specs/
fi

if [ ! -d "~/.local/share/gtksourceview-4/styles/" ]; then
	mkdir -p ~/.local/share/gtksourceview-4/styles/
fi

cp a_script_highlight.lang ~/.local/share/gtksourceview-4/language-specs/
cp a_dark.xml ~/.local/share/gtksourceview-4/styles/
cp a_light.xml ~/.local/share/gtksourceview-4/styles/

cp sh.lang ~/.local/share/gtksourceview-4/language-specs/sh.lang
cp def.lang ~/.local/share/gtksourceview-4/language-specs/def.lang

# old path: /usr/share/gtksourceview-3.0/styles/
# cp gedit_separate_windows.desktop ~/.local/share/applications/
#dconf load / < settings.dconf

echo Done!
