Settings -> Show all -> Video -> Snapshot:
Video snapshot file prefix: vlcsnap-$T-

# From: https://wiki.videolan.org/Documentation:Snapshots/

// use it for radio too:
# BBC R3:
http://bbcmedia.ic.llnwd.net/stream/bbcmedia_radio3_mf_p
# ClassicFM
http://media-ice.musicradio.com/ClassicFM
# sources:
http://media-ice.musicradio.com/
http://www.suppertime.co.uk/blogmywiki/2015/04/updated-list-of-bbc-network-radio-urls/

// issue not playing some low quality mp4s:
https://bbs.archlinux.org/viewtopic.php?id=251235

// bugs: 
when sorting the Media Library according to a certain field, there is no way to revert to original sorting


